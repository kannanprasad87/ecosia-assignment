//
//  ViewController.swift
//  MyAudioPlayer
//
//  Created by Kannan Prasad  on 24/03/17.
//  Copyright © 2017 KEA. All rights reserved.
//

import UIKit
import AVFoundation

var audioPlayer: AVAudioPlayer!
var playerTimer: Timer!

class ViewController: UIViewController {
    
    @IBOutlet var slider: UISlider!
    @IBOutlet var trackLabel : UILabel!
    @IBOutlet var artistLabel : UILabel!
    @IBOutlet var artWorkimage : UIImageView!
    @IBOutlet var elapsedTimeLabel : UILabel!
    @IBOutlet var totalTimeLabel :UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slider.value = 0.0
        artWorkimage.layer.borderColor = UIColor.lightGray.cgColor
        artWorkimage.layer.borderWidth = 1.0
        artWorkimage.layer.cornerRadius = 3.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playSong()
    {
        let path = Bundle.main.path(forResource: "song.mp3", ofType:nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
           audioPlayer = try AVAudioPlayer(contentsOf: url)
        } catch
        {
            // couldn't load file :(
        }

        let playerItem = AVPlayerItem(url:url)
        let metadataList = playerItem.asset.metadata
        
        for item in metadataList {

            guard let key = item.commonKey, let value = item.value else{
                continue
            }
            
            switch key {
            case "title" : trackLabel.text = value as? String
            case "artist": artistLabel.text = value as? String
            case "artwork" where value is NSData :
                artWorkimage.image = UIImage(data: (value as! NSData) as Data)
            default:
                continue
            }
            
        }
        audioPlayer.prepareToPlay()
        slider.maximumValue = Float(audioPlayer.duration)
        totalTimeLabel.text = String(format:"%@",stringFromTimeInterval(interval:audioPlayer.duration))
        
        if playerTimer == nil{
        
            playerTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
        audioPlayer.play()
        
    }
    
    @IBAction  func stopPlay()
    {
        if audioPlayer != nil {
            audioPlayer.stop()
            playerTimer.invalidate()
        }
    }
  
    func updateTime(_ timer: Timer) {
        slider.value = Float(audioPlayer.currentTime)
        elapsedTimeLabel.text = String(format:"%@", stringFromTimeInterval(interval:audioPlayer.currentTime))
        print(audioPlayer.currentTime)
    }
    
    @IBAction func slide(_ slider: UISlider) {
        audioPlayer.currentTime = TimeInterval(slider.value)
        elapsedTimeLabel.text = String(format:"%@", stringFromTimeInterval(interval: audioPlayer.currentTime))
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        let timeInterval = NSInteger(interval)
        let seconds = timeInterval % 60
        let minutes = (timeInterval / 60) % 60
        
        return NSString(format: "%0.2d:%0.2d",minutes,seconds)
    }
    
}

